#!/bin/sh


function ntimes
{
	I=0
	for((I=0;I<50;I++))
	do
		"$@"
	done
}

function fail
{
	echo "FAIL: $1" >&2
	exit 1
}

# check output is always different
if [ -n "$(uuid -n 50 | uniq --repeated)" ]
then
	fail  "dulicated output"
fi

# check it can parse uuid it generated
if ! uuid -d $(uuid -v1) >/dev/null 2>&1
then
	fail "can't decore uuid"
fi

# check v3 uses same output for same seed 
if ! [ "$({ uuid -n 5 -v3 ns:URL https://fedoraproject.org/; uuid -n 5 -v3 ns:URL https://fedoraproject.org/ ; } | wc -l)" = 10 ]
then
	fail "v3 should have same output for same seed"
fi

# check v5 uses same output for same seed 
if ! [ "$({ uuid -n 5 -v3 ns:URL https://fedoraproject.org/; uuid -n 5 -v3 ns:URL https://fedoraproject.org/ ; } | wc -l)" = 10 ]
then
	fail "v5 should have same output for same seed"
fi

# check v3 and v5 use different hashes (output)
if ! [ "$({ uuid -n 5 -v3 ns:URL https://fedoraproject.org/; uuid -n 5 -v5 ns:URL https://fedoraproject.org/ ; } | uniq | wc -l)" = 2 ]
then
	fail "v3 and v5 uuid shouldn't be the same"
fi

echo "SUCCESS"
exit 0
